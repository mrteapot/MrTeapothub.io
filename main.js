window.onload = function () {
    window.addEventListener("scroll", onScroll);
}

hljs.initHighlightingOnLoad();

var docHeight = Math.round(document.body.offsetHeight);
var scrollFromTop;
var windowHeight;
var scrollPercentage;

function onScroll() {
    windowHeight = Math.round(window.innerHeight);
    scrollFromTop = Math.round(window.pageYOffset); 
    scrollPercentage = Math.round(scrollFromTop/(docHeight - windowHeight)*100);
    if (scrollPercentage > 100 ) scrollPercentage = 100;
    document.getElementById('readMeter').style.width = scrollPercentage + '%';
}

$('figure.highlight').each(function(i, block) {
    hljs.highlightBlock(block);
  });